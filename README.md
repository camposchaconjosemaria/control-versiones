# Control-Versiones

![git](img/git.jpg)


## ¿Qué es Git?

Es un software de control de versiones diseñado por [Linus Torvalds]
pensando en la eficiencia y la confiabilidad del mantenimiento de versiones de aplicaciones 
 cuando estas tienen un gran número de archivos de código fuente.

## ¿Porque usar un sistema de control de versiones como Git?

Un sistema de control de versiones como Git nos ayuda a guardar el historial de cambios y crecimiento de los archivos de nuestro proyecto.

En realidad, los cambios y diferencias entre las versiones de nuestros proyectos pueden tener similitudes, algunas veces los cambios pueden ser solo una palabra o una parte específica de un archivo específico. Git está optimizado para guardar todos estos cambios de forma atómica e incremental, o sea, aplicando cambios sobre los últimos cambios, estos sobre los cambios anteriores y así hasta el inicio de nuestro proyecto.

## [Comandos iniciales en Git](comandos.md)

